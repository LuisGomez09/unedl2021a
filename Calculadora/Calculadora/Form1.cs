﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(numero1.Text);
            int num2 = int.Parse(numero2.Text);
            int resultadoo = num1 + num2;
            result.Text = String.Concat(resultadoo);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(numero1.Text);
            int num2 = int.Parse(numero2.Text);
            int resultadoo = num1 - num2;
            result.Text = String.Concat(resultadoo);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int num1 = int.Parse(numero1.Text);
            int num2 = int.Parse(numero2.Text);
            int resultadoo = num1 * num2;
            result.Text = String.Concat(resultadoo);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
 
            if ((double.Parse(numero2.Text) == 0) || (double.Parse(numero1.Text) == 0))
            {
                MessageBox.Show("No se puede dividir entre 0");
                numero1.Clear();
                numero2.Clear();
            }
            else
            {
                int num1 = int.Parse(numero1.Text);
                int num2 = int.Parse(numero2.Text);
                int resultadoo = num1 / num2;
                result.Text = String.Concat(resultadoo);
            }
        }
    }
}
