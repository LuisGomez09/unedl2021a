﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsyncBreakfast
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ConsoleApp1.Coffee cup = PourCoffee();
            Console.WriteLine("el café está listo");

            var eggsTask = FryEggsAsync(2);
            var baconTask = FryBaconAsync(3);
            var toastTask = MakeToastWithButterAndJamAsync(2);

            var breakfastTasks = new List<Task> { eggsTask, baconTask, toastTask };
            while (breakfastTasks.Count > 0)
            {
                Task finishedTask = await Task.WhenAny(breakfastTasks);
                if (finishedTask == eggsTask)
                {
                    Console.WriteLine("los huevos están listos");
                }
                else if (finishedTask == baconTask)
                {
                    Console.WriteLine("el tocino está listo");
                }
                else if (finishedTask == toastTask)
                {
                    Console.WriteLine("la tostada está lista");
                }
                breakfastTasks.Remove(finishedTask);
            }

            ConsoleApp1.Juice oj = PourOJ();
            Console.WriteLine("oj está listo");
            Console.WriteLine("El desayuno esta listo!");
        }

        static async Task<ConsoleApp1.Toast> MakeToastWithButterAndJamAsync(int number)
        {
            var toast = await ToastBreadAsync(number);
            ApplyButter(toast);
            ApplyJam(toast);

            return toast;
        }

        private static ConsoleApp1.Juice PourOJ()
        {
            Console.WriteLine("Verter jugo de naranja");
            return new ConsoleApp1.Juice();
        }

        private static void ApplyJam(ConsoleApp1.Toast toast) =>
            Console.WriteLine("Poner mermelada en la tostada");

        private static void ApplyButter(ConsoleApp1.Toast toast) =>
            Console.WriteLine("Poner mantequilla en la tostada");

        private static async Task<ConsoleApp1.Toast> ToastBreadAsync(int slices)
        {
            for (int slice = 0; slice < slices; slice++)
            {
                Console.WriteLine("Poner una rebanada de pan en la tostadora");
            }
            Console.WriteLine("Empiece a tostar...");
            await Task.Delay(3000);
            Console.WriteLine("Retire la tostada de la tostadora.");

            return new ConsoleApp1.Toast();
        }

        private static async Task<ConsoleApp1.Bacon> FryBaconAsync(int slices)
        {
            Console.WriteLine($"poniendo {slices} rebanadas de tocino en la sartén");
            Console.WriteLine("cocinar el primer lado del tocino...");
            await Task.Delay(3000);
            for (int slice = 0; slice < slices; slice++)
            {
                Console.WriteLine("voltear una rebanada de tocino");
            }
            Console.WriteLine("cocinar el segundo lado del tocino...");
            await Task.Delay(3000);
            Console.WriteLine("Poner tocino en un plato");

            return new ConsoleApp1.Bacon();
        }

        private static async Task<ConsoleApp1.Egg> FryEggsAsync(int howMany)
        {
            Console.WriteLine("Calentar la sartén para huevos...");
            await Task.Delay(3000);
            Console.WriteLine($"agrietamiento {howMany} huevos");
            Console.WriteLine("cocinar los huevos ...");
            await Task.Delay(3000);
            Console.WriteLine("Pon los huevos en un plato");

            return new ConsoleApp1.Egg();
        }

        private static ConsoleApp1.Coffee PourCoffee()
        {
            Console.WriteLine("Verter el café");
            return new ConsoleApp1.Coffee();
        }
    }
}
